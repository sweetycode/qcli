#coding=utf8

import qiniu

from .utils import cached_property


class Bucket(object):
    def __init__(self, access_key, secret_key, bucket):
        self.access_key = access_key
        self.secret_key = secret_key
        self.bucket = bucket

    @cached_property
    def _auth(self):
        return qiniu.Auth(self.access_key, self.secret_key)

    @cached_property
    def _upload_token(self):
        return self._auth.upload_token(self.bucket, None, 3600)

    @cached_property
    def _manager(self):
        return qiniu.BucketManager(self._auth)

    def stat(self, key):
        return self._manager.stat(self.bucket, key)

    def ls(self, prefix, limit):
        return self._manager.list(self.bucket, prefix=prefix, limit=limit)

    def mv(self, key, dest_key, dest_bucket=None):
        return self._manager.move(self.bucket, key, dest_bucket or self.bucket, dest_key)

    def cp(self, key, dest_key, dest_bucket=None):
        return self._manager.copy(self.bucket, key, dest_bucket or self.bucket, dest_key)

    def rm(self, key):
        return self._manager.delete(self.bucket, key)

    def fetch(self, url, key):
        return self._manager.fetch(url, self.bucket, key)

    def upload_file(self, fpath, key):
        return qiniu.put_file(self._upload_token, key, fpath)

    def upload_data(self, data, key):
        return qiniu.put_data(self._upload_token, key, data)
