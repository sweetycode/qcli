#coding=utf8
import os
import json
import re


def read_json_file(fpath):
    try:
        with open(fpath) as f:
            return json.loads(f.read())
    except IOError:
        return {}
    except ValueError:
        raise
        return {}


_json_file_cache = {}
def read_cached_json_file(fpath, use_cache=True):
    abspath = os.path.abspath(os.path.expanduser(fpath))
    if use_cache:
        try:
            return _json_file_cache[abspath]
        except KeyError:
            pass
    v = read_json_file(abspath)
    _json_file_cache[abspath] = v
    return v


def get_files_config(fpath_list, key):
    for fpath in fpath_list:
        v = read_cached_json_file(fpath)
        try:
            return v[key]
        except KeyError:
            pass

    return None


def list_files(path, recursive=False):
    if not recursive:
        return [(os.path.join(path, name), name) for name in os.listdir(path) if os.path.isfile(os.path.join(path, name))]
    else:
        if not path.endswith('/'):
            path += '/'
        prefix_len = len(path)

        rv = []
        for dirname, _, files in os.walk(path):
            prefix = dirname[prefix_len:]
            rv.extend([(os.path.join(path, dirname, name), os.path.join(prefix, name)) for name in files])

        return rv

class cached_property(object):
    """
    Decorator that converts a method with a single self argument into a
    property cached on the instance.
    """
    def __init__(self, func):
        self.func = func

    def __get__(self, instance, type=None):
        if instance is None:
            return self
        res = instance.__dict__[self.func.__name__] = self.func(instance)
        return res
