#coding=utf8
import os

import click
import qiniu

from .utils import get_files_config, list_files
from .qiniu_api import Bucket


"""
click.context
    auth info
    bucket info
"""

class Config(object):
    AUTH_FIELS = ['.qiniu_auth.conf', '.qiniu.conf', '.qiniu/auth.conf', '.qiniu/default.conf']
    BUCKET_FIELS = ['.qiniu_bucket.conf', '.qiniu.conf', '.qiniu/bucket.conf', '.qiniu/default.conf']
    def __init__(self, conf_file=None, bucket=None, access_key=None, secret_key=None):
        auth_files = self.AUTH_FIELS + ['~/'+f for f in self.AUTH_FIELS]
        if conf_file:
            auth_files.insert(0, conf_file)
        bucket_files = self.BUCKET_FIELS + ['~/'+f for f in self.BUCKET_FIELS]
        if conf_file:
            bucket_files.insert(0, conf_file)

        self.access_key = access_key or get_files_config(auth_files, 'access_key')
        self.secret_key = secret_key or get_files_config(auth_files, 'secret_key')
        self.bucket = bucket or get_files_config(bucket_files, 'bucket')

    def get_bucket(self):
        return Bucket(self.access_key, self.secret_key, self.bucket)


@click.group()
@click.option('--conf-file', '-c')
@click.option('--access-key', '-a')
@click.option('--secret-key', '-s')
@click.option('--bucket', '-b')
@click.pass_context
def cli(ctx, conf_file=None, access_key=None, secret_key=None, bucket=None):
    ctx.obj = Config(conf_file=conf_file, access_key=access_key, secret_key=secret_key, bucket=bucket)


@cli.command(help="status")
@click.pass_obj
def st(config):
    print 'access_key:', config.access_key
    print 'secret_key:', config.secret_key
    print 'bucket:', config.bucket


@cli.command()
@click.argument('key')
@click.pass_obj
def stat(config, key):
    bucket = config.get_bucket()
    rv = bucket.stat(key)
    print rv


@cli.command(help="upload from url")
@click.argument('url')
@click.argument('key', required=False)
@click.pass_obj
def fetch(config, url, key):
    bucket = config.get_bucket()
    if key is None:
        key = os.path.basename(url)

    rv = bucket.fetch(url, key)
    print rv


@cli.command(help="upload")
@click.argument('file-path')
@click.argument('prefix', required=False)
@click.argument('key', required=False)
@click.pass_obj
def up(config, file_path, prefix, key):
    bucket = config.get_bucket()

    upload_key = os.path.basename(file_path)
    if prefix:
        upload_key = prefix + os.path.basename(file_path)
    elif key:
        upload_key = key

    rv = bucket.upload_file(file_path, upload_key)
    print rv


@cli.command(help="upload dir")
@click.option('--recursive', '-r', default=False)
@click.argument('dir')
@click.argument('prefix', required=False)
@click.pass_obj
def updir(config, recursive, dir, prefix):
    bucket = config.get_bucket()
    if prefix is not None:
        upload_prefix = prefix
    else:
        upload_prefix = os.path.basename(os.path.abspath(dir)) + '/'

    for path, name in list_files(dir, recursive):
        rv = bucket.upload_file(path, upload_prefix + name)
        print rv


def run():
    cli()
